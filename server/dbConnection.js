const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const hError = console.error.bind(console, 'connection error:');
const hClose = console.log.bind(console, 'connection close:');
const hOpen = console.log.bind(console, 'connection open:');

const connectDB = () => {
  mongoose.connect('mongodb://127.0.0.1:27017/photoalbum', (err) => {
    if (err) {
      throw err;
    }
    const { connection } = mongoose;
    connection.on('error', hError);
    connection.on('close', hClose);
    connection.on('open', hOpen);
  });
};

const disconnectDB = (cb) => {
  const { connection } = mongoose;
  connection.removeAllListeners();
  connection.close()
    .then(() => cb());
};

mongoose.models = {};
mongoose.modelSchemas = {};

module.exports = {
  createConnetion: () => {
    if (mongoose.connection.readyState === 0) {
      connectDB();
    } else {
      disconnectDB(connectDB);
    }
    return mongoose.connection;
  },
};
