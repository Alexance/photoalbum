const mongoose = require('mongoose');

const Photo = new mongoose.Schema({
  id: String,
  name: String,
  fileName: String,
  created: { type: Date, default: Date.now },
  likes: { type: Number, default: 0 },
}, { versionKey: false });

Photo.pre('save', function (next) {
  this.id = this._id;
  next();
});

module.exports = mongoose.model('Photo', Photo);
