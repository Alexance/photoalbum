const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Album = new Schema({
  id: String,
  name: String,
  photos: [{ type: Schema.Types.ObjectId, ref: 'Photo' }],
  preview: {
    type: Schema.Types.ObjectId,
    ref: 'Photo',
  },
  description: String,
  created: { type: Date, default: Date.now },
});

Album.pre('save', function (next) {
  this.id = this._id;
  next();
});

module.exports = mongoose.model('Album', Album);
