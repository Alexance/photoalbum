const fs = require('fs');
const path = require('path');
const debug = require('debug')('app:server');
const { NO_CONTENT, INTERNAL_SERVER_ERROR } = require('http-status');

const Photo = require('../../model/photo');

// @TODO: create separate file with all path constants
const UPLOADS_PATH = path.resolve(__dirname, '../../../uploads/');
/**
 * Get method for photos list
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = (req, res) => {
  const { id } = req.params;
  Photo.findOneAndRemove({ _id: id })
    .then(() => {
      fs.unlink(
        path.join(UPLOADS_PATH, `${id}`),
        (err) => {
          const status = err ? INTERNAL_SERVER_ERROR : NO_CONTENT;
          res.sendStatus(status);
        });
    })
    .catch((err) => {
      debug(err);
      res.sendStatus(500);
    });
};

module.exports = method;
