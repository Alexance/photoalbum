const router = require('express').Router();

const get = require('./get');
const deleteMethod = require('./delete');

router
  .get('/', get)
  .delete('/:id', deleteMethod);

module.exports = router;
