const debug = require('debug')('app:server');
const { OK, INTERNAL_SERVER_ERROR } = require('http-status');
const Photo = require('../../model/photo');

/**
 * Get method for photos list
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = (req, res) => {
  Photo.find({}, { _id: false, __v: false })
    .then((photos) => {
      res.status(OK).json(photos);
    })
    .catch((err) => {
      debug(err);
      res.sendStatus(INTERNAL_SERVER_ERROR);
    });
};

module.exports = method;
