const debug = require('debug')('app:server');
const Album = require('../../model/album');
const { OK, BAD_REQUEST } = require('http-status');

/**
 * @api {get} /api/albums get all albums
 * @apiGroup Albums
 * @apiName get
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = async (req, res) => {
  try {
    const albums = await Album.find({}).populate('photos').exec();
    return res.status(OK).json(albums);
  } catch (err) {
    debug(err);
    return res.status(BAD_REQUEST);
  }
};

module.exports = method;
