const debug = require('debug')('app:server');
const Album = require('../../model/album');
const { OK, BAD_REQUEST } = require('http-status');

/**
 * @api {get} /api/albums/:id get one album
 * @apiGroup Albums
 * @apiName getById
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = async (req, res) => {
  try {
    const { params : { id } } = req;
    const album = await Album.findOne({ id }).populate('photos').exec();
    if (!album) {
      return res.sendStatus(BAD_REQUEST);
    }
    return res.status(OK).json(album);
  } catch (err) {
    debug(err);
    return res.sendStatus(BAD_REQUEST);
  }
};

module.exports = method;
