const router = require('express').Router();
const bodyParser = require('body-parser');
const { BAD_REQUEST } = require('http-status');

const get = require('./get');
const getById = require('./getById');
const post = require('./post');
const deleteById = require('./deleteById');
const put = require('./put');

const checkBody = (req, res, next) => {
  const { body } = req;
  if (body.name && body.name !== '') {
    next();
  }
  return res.status(BAD_REQUEST);
};

router
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  .get('/', get)
  .get('/:id', getById)
  .post('/', checkBody, post)
  .delete('/:id', deleteById)
  .put('/:id', checkBody, put);

module.exports = router;
