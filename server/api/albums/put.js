const debug = require('debug')('app:server');
const Album = require('../../model/album');
const { NO_CONTENT, BAD_REQUEST } = require('http-status');

/**
 * @api {put} /api/albums/:id update a existing album
 * @apiGroup Albums
 * @apiName put
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = async (req, res) => {
  const { body } = req;
  const { params : { id } } = req;
  delete body.id;
  delete body._id;
  try {
    await Album.update({ id }, { $set: body });
  } catch (err) {
    debug(err);
    return res.status(BAD_REQUEST);
  }
  res.sendStatus(NO_CONTENT);
};

module.exports = method;
