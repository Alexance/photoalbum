const debug = require('debug')('app:server');
const Album = require('../../model/album');
const { NO_CONTENT, BAD_REQUEST } = require('http-status');

/**
 * @api {post} /api/albums create a new album
 * @apiGroup Albums
 * @apiName post
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = async (req, res) => {
  const { body } = req;
  try {
    const album = new Album(body);
    await album.save();
  } catch (err) {
    debug(err);
    return res.sendStatus(BAD_REQUEST);
  }
  res.sendStatus(NO_CONTENT);
};

module.exports = method;
