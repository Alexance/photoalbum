const debug = require('debug')('app:server');
const Album = require('../../model/album');
const { NO_CONTENT, BAD_REQUEST } = require('http-status');

/**
 * @api {delete} /api/albums/:id delete one album
 * @apiGroup Albums
 * @apiName delete
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = async (req, res) => {
  try {
    const { params: { id } } = req;
    const deletedItem = await Album.findOneAndRemove({ id });
    if (!deletedItem) {
      return res.sendStatus(BAD_REQUEST);
    }
  } catch (err) {
    debug(err);
    return res.sendStatus(BAD_REQUEST);
  }
  res.sendStatus(NO_CONTENT);
};


module.exports = method;
