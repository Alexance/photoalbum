const fs = require('fs');
const path = require('path');

// @TODO: create separate file with all path constants
const UPLOADS_PATH = path.resolve(__dirname, '../../../uploads/');

/**
 * Get upload by its id
 *
 * @param  {Object} req Request object
 * @param  {Object} res Response object
 */
const method = (req, res) => {
  const fullPath = path.join(UPLOADS_PATH, req.params.id);
  fs.exists(fullPath, (exists) => {
    if (exists) {
      return res.status(200).sendFile(fullPath);
    }
    res.sendStatus(404);
  });
};

module.exports = method;
