const router = require('express').Router();

const getById = require('./getById');

router
  .get('/:id', getById);

module.exports = router;
