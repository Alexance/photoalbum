const path = require('path');
const multer = require('multer');
const debug = require('debug')('app:server');
const { NO_CONTENT, INTERNAL_SERVER_ERROR } = require('http-status');
const Photo = require('../../model/photo');

const UPLOADS_PATH = path.resolve(__dirname, '../../../uploads/');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, UPLOADS_PATH);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname;
    const ext = path.extname(fileName);
    const name = path.basename(fileName, ext);
    const photo = new Photo({
      name,
      fileName,
    });
    photo.save()
      .then(x => cb(null, `${x.id}`))
      .catch(err => cb(err));
  },
});
const upload = multer({ storage }).single('file');

const method = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      debug('Error uploading file');
      return res.status(INTERNAL_SERVER_ERROR).end();
    }

    debug('File uploaded successfuly');
    return res.status(NO_CONTENT).end();
  });
};

module.exports = method;
