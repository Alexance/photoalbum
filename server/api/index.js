const router = require('express').Router();

const upload = require('./upload');
const photos = require('./photos');
const images = require('./images');
const albums = require('./albums');

router.use('/upload', upload);
router.use('/photos', photos);
router.use('/images', images);
router.use('/albums', albums);

module.exports = router;
