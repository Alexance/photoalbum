const chai = require('chai');
const chaiHttp = require('chai-http');
const proxyquire = require('proxyquire');

chai.should();
chai.use(chaiHttp);

const stubGet = (res, req) => req.sendStatus(200);
stubGet['@global'] = true;
const stubDelete = (res, req) => req.sendStatus(200);
stubDelete['@global'] = true;
const stubDbConnection = { createConnetion: () => { }, '@global': true };
const stubDebug = () => () => { };
stubDebug['@global'] = true;
const server = proxyquire('../../main', {
  './get': stubGet,
  './delete': stubDelete,
  './dbConnection': stubDbConnection,
  'debug': stubDebug,
});

const request = () => chai.request(server);

describe('API /api/photos/', () => {
  describe('GET /api/photos', () => {
    it('should call middleware "get"', (done) => {
      request()
        .get('/api/photos')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('should call retutn status 404 if url is excess', (done) => {
      request()
        .get('/api/photos/1')
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe('DELETE /api/photos/:id', () => {
    it('should call middleware "deleteMethod" if ":id" is', (done) => {
      request()
        .delete('/api/photos/1')
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });

    it('should call retutn status 404 ":id" isn\'t', (done) => {
      request()
        .delete('/api/photos')
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });
});
