const chai = require('chai');
const chaiHttp = require('chai-http');
const proxyquire = require('proxyquire');
const mock = require('./mock');
const sinon = require('sinon');
const path = require('path');

chai.should();
chai.use(chaiHttp);
const expect = chai.expect;

const UPLOADS_PATH = path.resolve(__dirname, '../../../uploads/');

const stubPhoto = { '@global': true };
const stubFs = { '@global': true };
const stubDbConnection = { createConnetion: () => { }, '@global': true };
const stubDebug = () => () => {};
stubDebug['@global'] = true;
const server = proxyquire('../../main', {
  '../../model/photo': stubPhoto,
  'fs': stubFs,
  './dbConnection': stubDbConnection,
  'debug': stubDebug,
});

const request = () => chai.request(server);

describe('API /api/photos/  METHOD DELETE', () => {
  const id0 = mock.photos[0].id;
  const id1 = mock.photos[1].id;
  const idWrong = '123';
  const sandbox = sinon.sandbox.create();
  let findOneAndRemove;
  let unlink;

  beforeEach(() => {
    findOneAndRemove = sandbox.stub();
    findOneAndRemove.withArgs({ _id: id0 }).resolves();
    findOneAndRemove.withArgs({ _id: id1 }).resolves();
    findOneAndRemove.withArgs({ _id: idWrong }).rejects();
    stubPhoto.findOneAndRemove = findOneAndRemove;
    unlink = sandbox.stub();
    unlink.withArgs(path.join(UPLOADS_PATH, id1)).callsFake((_, cd) => { cd(null); });
    unlink.withArgs(path.join(UPLOADS_PATH, id0)).callsFake((_, cd) => { cd('err'); });
    stubFs.unlink = unlink;
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('should be responseStatus 500 if doesn\'t remove a record from DB', (done) => {
    request()
      .delete(`/api/photos/${idWrong}`)
      .end((err, res) => {
        res.should.have.status(500);
        expect(findOneAndRemove.callCount).to.equal(1);
        expect(unlink.callCount).to.equal(0);
        done();
      });
  });

  it('should be responseStatus 500 if doesn\'t delete a file', (done) => {
    request()
      .delete(`/api/photos/${id0}`)
      .end((err, res) => {
        res.should.have.status(500);
        expect(findOneAndRemove.callCount).to.equal(1);
        expect(unlink.callCount).to.equal(1);
        done();
      });
  });

  it('should be responseStatus 204', (done) => {
    request()
      .delete(`/api/photos/${id1}`)
      .end((err, res) => {
        res.should.have.status(204);
        expect(findOneAndRemove.callCount).to.equal(1);
        expect(unlink.callCount).to.equal(1);
        done();
      });
  });
});
