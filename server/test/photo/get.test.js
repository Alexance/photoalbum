const chai = require('chai');
const chaiHttp = require('chai-http');
const proxyquire = require('proxyquire');
const mock = require('./mock');

chai.should();
chai.use(chaiHttp);

const stubPhoto = { find: () => Promise.resolve(), '@global': true };
const stubDbConnection = { createConnetion: () => { }, '@global': true };
const stubDebug = () => () => { };
stubDebug['@global'] = true;
const server = proxyquire('../../main', {
  '../../model/photo': stubPhoto,
  './dbConnection': stubDbConnection,
  'debug': stubDebug,
});

const request = () => chai.request(server);

describe('API /api/photos/  METHOD GET', () => {
  describe('Photos aren\'t', () => {
    before(() => {
      stubPhoto.find = () => Promise.resolve([]);
    });

    it('should return an empty array', (done) => {
      request()
        .get('/api/photos')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.should.be.eql([]);
          done();
        });
    });
  });

  describe('Photos are', () => {
    before(() => {
      stubPhoto.find = () => Promise.resolve(mock.photos);
    });

    it('should return a not empty array', (done) => {
      request()
        .get('/api/photos')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.should.be.eql(mock.photos);
          done();
        });
    });
  });
});
