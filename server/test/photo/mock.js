module.exports.photos = [
  {
    id: '58ef74bf305da50ebcab39cc',
    name: 'Field-of-Wheat',
    fileName: 'Field-of-Wheat.jpg',
    likes: 0,
    created: '2017-04-13T12:53:19.562Z',
  },
  {
    id: '58ef80f43713e30d10a62707',
    name: 'Mountain-Lake',
    fileName: 'Mountain-Lake.jpg',
    likes: 0,
    created: '2017-04-13T13:45:24.987Z',
  },
  {
    id: '58ef83658418091da0c00a87',
    name: 'Cherries-In-Ice',
    fileName: 'Cherries-In-Ice.jpg',
    likes: 0,
    created: '2017-04-13T13:55:49.247Z',
  },
  {
    id: '58ef836a8418091da0c00a88',
    name: 'Winter-Snow-House',
    fileName: 'Winter-Snow-House.jpg',
    likes: 0,
    created: '2017-04-13T13:55:54.325Z',
  },
];
