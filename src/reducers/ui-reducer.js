// import Popups from '../models/popups';
import UI from '../models/ui';
import { SHOW_POPUP, HIDE_POPUP } from '../actions/popups';

const initialState = new UI();

export default function ui(state = initialState, action) {
  switch (action.type) {
    case SHOW_POPUP:
      return state.setPopups(state.getPopups().setPopupVisibility(action.id, true));
    case HIDE_POPUP:
      return state.setPopups(state.getPopups().setPopupVisibility(action.id, false));
    default:
      return state;
  }
}
