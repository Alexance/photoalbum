import { ADD_PHOTOS, LIKE_PHOTO, DISLIKE_PHOTO, EDIT_PHOTO } from '../actions/photos';

import Photos from '../models/photos';

const initialState = new Photos();
/**
 * @function {likePhoto} increase photo's like by one
 * @param {object} state app state
 * @param {id} id id of liked photo
 */
function likePhoto(state, id) {
  const photo = state.getById(id).incrementLikes();
  return state.updatePhoto(photo);
}
/**
 * @function {likePhoto} reduce photo's like by one
 * @param {object} state app state
 * @param {id} id id of disliked photo
 */
function dislikePhoto(state, id) {
  const photo = state.getById(id).decrementLikes();
  return state.updatePhoto(photo);
}
/**
 * @function {editPhoto} reduce photo's like by one
 * @param {object} state app state
 * @param {id} id id of disliked photo
 * @param {object} photo object with data for editing
 */
function editPhoto(state, id, photo) {
  const newPhoto = state.getById(id).setName(photo.name);
  return state.updatePhoto(newPhoto);
}

export default function photos(state = initialState, action) {
  switch (action.type) {
    case ADD_PHOTOS:
      return Photos.fromResponse(action.photos);
    case EDIT_PHOTO:
      return editPhoto(state, action.id, action.photo);
    case LIKE_PHOTO:
      return likePhoto(state, action.id);
    case DISLIKE_PHOTO:
      return dislikePhoto(state, action.id);
    default:
      return state;
  }
}
