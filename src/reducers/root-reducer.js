import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import photos from './photos-reducer';
import ui from './ui-reducer';

const reducers = {
  routing: routerReducer,
  form: formReducer,
  photos,
  ui,
};

export default function createReducer() {
  return combineReducers({ ...reducers });
}
