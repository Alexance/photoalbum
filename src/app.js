/* eslint-disable */
import React from 'react';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import ReactDOM from 'react-dom';

import configureStore from './store/create-store.js';
import { syncHistoryWithStore } from 'react-router-redux';
import createRoutes from './routes';

const initialState = {};
const store = configureStore(initialState, browserHistory);
const history = syncHistoryWithStore(browserHistory, store);
const routes = createRoutes();

ReactDOM.render (
  <Provider store={store}>
    <Router history={history}>
      {routes}
    </Router>
  </Provider>,
  document.getElementById('root')
); 
