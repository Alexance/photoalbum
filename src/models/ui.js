import { Record } from 'immutable';
import Popups from './popups';

/**
* @const Parent class for UI data model
*/
const Parent = Record({
  popups: new Popups(),
}, 'UI');

/**
* UI data model with getters and setters methods
* @extends Parent
*/
export default class UI extends Parent {
  /**
  * @method {getPopups} getting popups of the UI object
  */
  getPopups() {
    return this.get('popups');
  }
  /**
  * @method {setPopups} setting popups of the UI object
  * @param {object} popups object with Popup's objects
  */
  setPopups(popups) {
    return this.set('popups', popups);
  }
}
