import { Record, Map } from 'immutable';

/**
* @const Parent class for Albums data model
*/
const Parent = Record({
  name: null,
  photos: Map(),
  preview: null,
  description: null,
  created: null,
}, 'Albums');

/**
* Albums data model with getters and setters methods
* @extends Parent
*/
export default class Albums extends Parent {
  /**
  * @method {getName} getting name of the album
  */
  getName() {
    return this.get('name');
  }

  /**
  * @method {getPhotos} getting array of photos from the album
  */
  getPhotos() {
    return this.get('photos');
  }

  /**
  * @method {getPreview} getting id of the photo preview from the album
  */
  getPreview() {
    return this.get('preview');
  }

  /**
  * @method {getDescription} getting description of the album
  */
  getDescription() {
    return this.get('description');
  }

  /**
  * @method {getCreated} getting created date of the album
  */
  getCreated() {
    return this.get('created');
  }

  /**
  * @method {setName} setting name to the album
  * @param {string} name A new name setting to the album
  */
  setName(name) {
    return this.set('name', name);
  }

  /**
  * @method {setPhotos} setting photos id array to the album
  * @param {array} photos  An array setting to the album
  */
  setPhotos(photos) {
    return this.get('photos', photos);
  }

  /**
  * @method {addPhoto} adding photo's id to the album's photos array
  * @param {string} id A id setting to the album's photos array
  */
  addPhoto(id) {
    return this.setIn(['photos', id], id);
  }

  /**
  * @method {deletePhoto} deleting photo's id from the album's photos array
  * @param {string} id A id of deleted photo
  */
  deletePhoto(id) {
    return this.deleteIn(['photos', id]);
  }

  /**
  * @method {setPreview} setting preview to the album
  * @param {string} preview An preview's id setting to the album
  */
  setPreview(preview) {
    return this.set('preview', preview);
  }

  /**
  * @method {setDescription} setting description to the album
  * @param {string} description An description text setting to the album
  */
  setDescription(description) {
    return this.set('description', description);
  }

  /**
  * @method {setCreated} setting created date to the album
  * @param {string} createdDate A created date setting to the album
  */
  setCreated(createdDate) {
    return this.set('created', createdDate);
  }
}
