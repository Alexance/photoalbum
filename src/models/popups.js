import { Record, Map } from 'immutable';
/**
* @const Parent class for popups data model
*/
const Parent = Record({
  popups: Map(),
}, 'Popups');


/**
* UI data model with getters and setters methods
* @extends Parent
*/
export default class Popups extends Parent {
  /**
   * @method {getPopupVisibility} getting isVisible prop of popup by id
   * @param {string} id popup's id
   */
  getPopupVisibility(id) {
    return this.getIn(['popups', id], false);
  }

  /**
  * @method {setPopupVisibility} setting isVisible prop of popup
  * @param {string} id popup's id
  * @param {boolean} visibility popup's isVisible prop to set
  */
  setPopupVisibility(id, visibility) {
    return this.setIn(['popups', id], visibility);
  }
}
