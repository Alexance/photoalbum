import { Record } from 'immutable';
/**
* @const Parent class for Photo data model
*/
const Parent = Record({
  id: null,
  name: null,
  fileName: null,
  created: null,
  likes: 0,
}, 'Photo');
/**
* Photo data model with getters and setters methods
* @extends Parent
*/
export default class Photo extends Parent {
  /**
  * @method {getId} getting id of the photo from the Photo's object
  */
  getId() {
    return this.get('id');
  }
  /**
  * @method {getName} getting name of the photo from the Photo's object
  */
  getName() {
    return this.get('name');
  }
  /**
  * @method {getFileName} getting file name of the photo from the Photo's object
  */
  getFilename() {
    return this.get('fileName');
  }
  /**
  * @method {getCreationDate} getting the date of the photo's creation from the Photo's object
  */
  getCreationDate() {
    return this.get('created');
  }
  /**
  * @method {getLikes} getting the value of photo's likes from the Photo's object
  */
  getLikes() {
    return this.get('likes');
  }
  /**
  * @method {setname} setting the name to the photo
  * @param {string} newName A new name setting to the photo
  */
  setName(newName) {
    return this.set('name', newName);
  }
  /**
  * @method {setFileName} setting the file name to the photo
  * @param {string} newFileName A new file name setting to the photo
  */
  setFileName(newFileName) {
    return this.set('fileName', newFileName);
  }
  /**
  * @method {incrementLikes} increment by one the value of photo's likes
  */
  incrementLikes() {
    const likes = this.getLikes();
    return this.set('likes', likes + 1);
  }
  /**
  * @method {decrementLikes} decrement by one the value of photo's likes
  */
  decrementLikes() {
    const likes = this.getLikes();
    return this.set('likes', Math.max(0, likes - 1)); // likes should be equal or greater than 0
  }
}

