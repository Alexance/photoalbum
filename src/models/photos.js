import { Record, List, OrderedMap } from 'immutable';
import Photo from './photo';
/**
* @const Parent class for Photos data model
*/
const Parent = Record({ photos: List() }, 'Photos');

/**
* Photos data model with getter and setters
* @extends Parent
*/
export default class Photos extends Parent {
  /**
  * @method {getPhotos} getting array of the Photo's objects
  */
  getPhotos() {
    return this.get('photos');
  }

  /**
  * @method {setPhotos} setting new array of Photo's objects
  * @param {array} photos new array of Photo's objects
  */
  setPhotos(photos) {
    return this.set('photos', photos);
  }

  /**
  * @method {getById} getting Photo's object by id
  * @param {number} id id of the Photo's object
  */
  getById(id) {
    const photos = this.getPhotos();
    return photos.find((photo => photo.getId() === id));
  }

  /**
  * @method {updatePhoto} updating Photo object
  * @param {object} photo updated Photo object
  */
  updatePhoto(photo) {
    const id = photo.getId();
    const photos = this.getPhotos();

    return this.setPhotos(photos.set(id, photo));
  }

  /**
  * @method {getRecentPhotos} getting the array of recent Photo's object sorted by date
  * @param {number} numberOfRecent the length of needed array
  */
  getRecentPhotos(numberOfRecent) {
    return this.getPhotos()
    .sort((photo1, photo2) => {
      const photo1Date = new Date(photo1.getCreationDate()).getTime();
      const photo2Date = new Date(photo2.getCreationDate()).getTime();

      return photo2Date - photo1Date;
    })
    .take(numberOfRecent);
  }

  /**
  * @method {fromResponse} create Photos model from server data
  * @param {array} data An array with photo's objets data
  */
  static fromResponse(data) {
    return new Photos({
      photos: OrderedMap().withMutations((ctx) => {
        data.forEach((p) => {
          const photo = new Photo(p);
          const id = photo.getId();
          ctx.set(id, photo);
        });
      }),
    });
  }
}
