export const SHOW_POPUP = 'SHOW_POPUP';
export const HIDE_POPUP = 'HIDE_POPUP';
/**
* @function {showPopup} open popup with current id
* @param {string} id popup's id
*/
export function showPopup(id) {
  return {
    type: SHOW_POPUP,
    id,
  };
}
/**
* @function {hidePopup} close popup with current id
* @param {string} id popup's id
*/
export function hidePopup(id) {
  return {
    type: HIDE_POPUP,
    id,
  };
}
