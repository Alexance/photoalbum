import API from '../api';

export const ADD_PHOTOS = 'ADD_PHOTOS';
export const LIKE_PHOTO = 'LIKE_PHOTO';
export const DISLIKE_PHOTO = 'DISLIKE_PHOTO';
export const EDIT_PHOTO = 'EDIT_PHOTO';

/**
 * @function {addPhotos} adding array of photo's objects to store
 * @param {array} photosList array of photo's objects
 */
export function addPhotos(photosList) {
  const photos = photosList.data;
  return {
    type: ADD_PHOTOS,
    photos,
  };
}

/**
* @function {getPhotosData} fetching photo's data from server
*/
export function getPhotosData() {
  return dispatch => API.photos.getPhotosData()
    .then(data => dispatch(addPhotos(data)))
    .catch(error => console.log(error));
}
/**
* @function {loadPhoto} loading photo from server
* @param {number} id id of loaded photo
*/
export function loadPhoto(id) {
  return API.photos.get(id)
    .then(data => data.data)
    .catch(error => console.log(error));
}

/**
* @function {uploadAndFetchPhotos} uploading photo to the server and fetching photos data from the server
* @param {object} photo
*/
export function uploadAndFetchPhotos(photo) {
  return dispatch => API.photos.post(photo)
    .then(() => dispatch(getPhotosData()))
    .catch(error => console.log(error));
}

/**
* @function {editPhoto} increacing photo's like to one
* @param {number} id id of photo
* @param {object} photo data for editing
*/
export function editPhoto(id, photo) {
  return {
    type: EDIT_PHOTO,
    id,
    photo,
  };
}

/**
* @function {likePhoto} increacing photo's like to one
* @param {number} id id of photo
*/
export function likePhoto(id) {
  return {
    type: LIKE_PHOTO,
    id,
  };
}

/**
* @function {dislikePhoto} reducing photo's like to one
* @param {number} id id of loaded photo
*/
export function dislikePhoto(id) {
  return {
    type: DISLIKE_PHOTO,
    id,
  };
}
