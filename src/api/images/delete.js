import axios from 'axios';
import endpoints from '../../constants/endpoint';

export default function _delete(id) {
  const path = `${endpoints.images}${id}`;
  return axios.delete(path);
}
