import axios from '../api';
import endpoints from '../../constants/endpoint';

export default function get(id) {
  const path = `${endpoints.images}${id}`;
  return axios.get(path);
}
