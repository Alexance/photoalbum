import get from './get';
import _delete from './delete';

const images = {
  get,
  delete: _delete,
};

export default images;
