import photos from './photos';
import images from './images';

const API = {
  photos,
  images,
};

export default API;
