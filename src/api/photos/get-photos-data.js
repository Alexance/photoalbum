import axios from '../api';
import endpoints from '../../constants/endpoint';

export default function getPhotosData() {
  const path = `${endpoints.photos}`;
  return axios.get(path);
}
