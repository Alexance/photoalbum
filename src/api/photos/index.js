import get from './get';
import _delete from './delete';
import getPhotosData from './get-photos-data';
import post from './post';

const photos = {
  get,
  post,
  delete: _delete,
  getPhotosData,
};

export default photos;
