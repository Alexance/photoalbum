import axios from '../api';
import endpoints from '../../constants/endpoint';

export default function _delete(id) {
  const path = `${endpoints.photos}${id}`;
  return axios.delete(path);
}
