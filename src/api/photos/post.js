import axios from '../api';
import endpoints from '../../constants/endpoint';

export default function post(data) {
  const path = `${endpoints.upload}`;
  return axios.post(path, data);
}
