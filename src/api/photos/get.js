import axios from '../api';
import endpoints from '../../constants/endpoint';

export default function get(id) {
  const path = `${endpoints.photos}${id}`;
  return axios.get(path);
}
