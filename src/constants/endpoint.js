/**
 * @const {object}
 * @property {string} base base path to the api
 * @property {string} images relative path for the images
 * @property {string} photos relative path for the photos
 * @property {string} photo relative path for the photo
 */
const endpoints = {
  base: 'http://localhost:3000/api',
  upload: '/upload/',
  images: '/images/',
  settings: '/settings/',
  photos: '/photos/',
  photo: '/photo/',
};

export default endpoints;
