/**
 * @const {object}
 * @property {string} loadPopup id for LoadPopup component
 */

const popupsId = {
  loadPopup: 'load-popup',
};

export default popupsId;
