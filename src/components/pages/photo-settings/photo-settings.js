import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';

import { editPhoto } from '../../../actions/photos';

import './photo-settings.scss';
/**
* PhotoSettings component for photo's settings
* @extends Component
*/
export class PhotoSettings extends Component {
  /**
  * @method {handleSubmit} submit form handler
  * @param {SytheticEvent} e
  */
  @autobind
  handleSubmit(e) {
    e.preventDefault();
    const id = this.props.params.imgName;
    const photo = {
      name: this.photoName.value,
    };
    this.props.editPhoto(id, photo);
  }

  render() {
    return (
      <main className="photo-settings">
        <div className="photo-settings__header">
          <h2 className="photo-settings__title">Photo Settings</h2>
          <p className="photo-settings__title-label">
            Please, fill the form to edit the photo!
          </p>
        </div>
        <form className="photo-settings__form" onSubmit={this.handleSubmit}>
          <label htmlFor="photo-name" className="photo-settings__name-input-label">
            Name of the photo
          </label>
          <Field name="photo-name" className="photo-settings__name-input"
                 component="input" type="text" ref={photoName => (this.photoName = photoName)}
          />
          <input className="photo-settings__submit-btn" type="submit" ref={submit => (this.submit = submit)} />
        </form>
      </main>
    );
  }
}

/**
* propTypes
* @type {object}
* @property {object} photos object with photos
* @property {function} editPhoto action for editing photo's data
*/
PhotoSettings.propTypes = {
  photos: React.PropTypes.object,
  editPhoto: React.PropTypes.func,
};

function mapStateToProps(state, ownProps) {
  return {
    photos: state.photos,
    initialValues: {
      'photo-name': state.photos.getById(ownProps.params.imgName).getName(),
    },
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ editPhoto }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm({ form: 'photo-settings' })(PhotoSettings));
