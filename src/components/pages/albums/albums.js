import React, { Component } from 'react';
import { connect } from 'react-redux';
import AlbumPreview from '../../blocks/previews/album-preview/album-preview';

import './albums.scss';
/**
 * Albums component for representing albums with images
 * @extends Component
 */
export class Albums extends Component {
  /**
  * render
  * @return {Albums} markup
  */
  render() {
    return (
      <main className="albums">
         <h2 className="albums__title">Albums</h2>
         <div className="albums__albums-container">
           {
             // @TODO create 'albums' property from 'albums' store property after back-end will be ready
             // change photo's getters to album's getters
             // temporary solution for demostration UI
             this.props.albums.map(photo => (
               <AlbumPreview
                 name={photo.getName()}
                 preview={photo.getId()}
                 description={photo.getCreationDate()}
               />
             ))
           }
         </div>
      </main>
    );
  }
}
// @TODO create 'albums' property from 'albums' store property after back-end will be ready
// temporary solution for demostration UI
function mapStateToProps(state) {
  return {
    albums: state.photos.getPhotos(),
  };
}

export default connect(mapStateToProps)(Albums);
