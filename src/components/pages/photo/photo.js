import React, { Component } from 'react';
import endpoints from '../../../constants/endpoint';
import './photo.scss';

/**
 * Photo component for representing full-sized image
 * @extends Component
 */
export class Photo extends Component {
  render() {
    /**
    * @constant
    * @type {string}
    */
    const photoPath = `${endpoints.base}${endpoints.images}${this.props.params.imgName}`;
    /**
    * render
    * @return {Photo} markup
    */
    return (
      <main className="photo">
         <h2>Full-sized photo</h2>
         <div className="photo__container">
           <img className="photo__img" src={photoPath} alt="photo"/>
         </div>
      </main>
    );
  }
}

export default Photo;
