import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import popupsId from '../../../constants/popups';
import CommonPopup from '../../common/popups/common-popup';
import LoadPopup from '../../common/popups/load-popup/load-popup';
import PhotoPreview from '../../blocks/previews/photo-preview/photo-preview';
import { hidePopup } from '../../../actions/popups';

import './homepage.scss';
/**
* Homepage component for representing home page content
* @extends Component
*/
export class Homepage extends Component {
  /**
  * render
  * @return {Homepage} markup
  */
  render() {
    return (
      <main className="homepage">
         <h2 className="homepage__title">Homepage</h2>
         <div className="homepage__photos-container">
           {
             this.props.photos.map(photo => (
                 <PhotoPreview
                   key={photo.getId()}
                   id={photo.getId()}
                   name={photo.getName()}
                   date={photo.getCreationDate()}
                   likes={photo.getLikes()}
                   likePhoto={this.props.likePhoto}
                 />
               ))
          }
         </div>
         <CommonPopup
           id={popupsId.loadPopup}
           isVisible={this.props.isVisible}
           hidePopup={this.props.hidePopup}
         >
           <LoadPopup />
         </CommonPopup>
      </main>
    );
  }
}

/**
* propTypes
* @type {object}
* @property {object} photos object of photos data
* @property {boolean} isVisible visibility of CommonPopup
* @property {func} hidePopup function to hide popup
*/
PhotoPreview.propTypes = {
  photos: React.PropTypes.object,
  isVisible: React.PropTypes.bool,
  hidePopup: React.PropTypes.func,
};

function mapStateToProps(state) {
  return {
    photos: state.photos.getPhotos(),
    isVisible: state.ui.getPopups().getPopupVisibility(popupsId.loadPopup),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ hidePopup }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
