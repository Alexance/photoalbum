import React, { Component } from 'react';

import './categories.scss';
/**
* Categories component for representing images related to appropriated categories
* @extends Component
*/
export class Categories extends Component {
  /**
  * render
  * @return {Categories} markup
  */
  render() {
    return (
      <main className="categories">
         <h2>Categories</h2>
      </main>
    );
  }
}

export default Categories;
