import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getPhotosData } from '../actions/photos';
import PageWrapper from './blocks/page-wrapper/page-wrapper';

import './photoalbum.scss';
/**
 * Photoalbum hight-level component for representing whole app content
 * @extends Component
 */
export class Photoalbum extends Component {
  // @TODO: temporary solution. Change after implementing authoriazation
  componentDidMount() {
    this.props.getPhotosData();
  }
  /**
  * render
  * @return {Photoalbum} markup
  */
  render() {
    return (
      <div className="photoalbum">
        <PageWrapper>
          {this.props.children}
        </PageWrapper>
      </div>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {object} children object with child nodes
* @property {function} getPhotosData function for receiving photos data
*/
Photoalbum.propTypes = {
  children: React.PropTypes.object,
  getPhotosData: React.PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    getPhotosData: bindActionCreators(getPhotosData, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(Photoalbum);
