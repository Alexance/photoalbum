import React, { Component } from 'react';

import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { showPopup } from '../../../actions/popups';
import popupsId from '../../../constants/popups';


import './upload-btn.scss';

/**
 * Component button for Uploading photos
 * @extends Component
 */
export class UploadBtn extends Component {
  /**
  * @method {onLoadBtnClick} btn click handler, shows load popup
  */
  @autobind
  onLoadBtnClick() {
    this.props.showPopup(popupsId.loadPopup);
  }
  /**
  * render
  * @return {UploadBtn} markup
  */
  render() {
    return (
      <button className="upload-photo-btn" onClick={this.onLoadBtnClick}>
        Upload photo
      </button>
    );
  }
}

/**
* propTypes
* @type {object}
* @property {func} showPopup function to show popup
*/
UploadBtn.propTypes = {
  showPopup: React.PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    showPopup: bindActionCreators(showPopup, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(UploadBtn);
