import React, { Component } from 'react';
import autobind from 'autobind-decorator';

import './search.scss';
/**
 * Search component for searching images
 * @extends Component
 */

export class Search extends Component {
  /**
  * handle submit form event
  * @param {SytheticEvent} e
  */
  @autobind
  onFormSubmit(e) {
    e.preventDefault();
  }
  /**
  * render
  * @return {Search} markup
  */
  render() {
    return (
      <div className="search">
        <form className="search-form" action="" onSubmit={this.onFormSubmit}>
          <input type="search" className="search-form__input" placeholder="Search"/>
        </form>
      </div>
    );
  }
}


export default Search;
