import React, { Component } from 'react';
import Header from '../../common/header/header';
import AsideMenu from '../../common/aside-menu/aside-menu';

import './page-wrapper.scss';
/**
* Page wrapper component for implementing page layout
* @extends Component
*/
export class PageWrapper extends Component {
  /**
  * render
  * @return {pageWrapper} markup
  */
  render() {
    return (
      <div className="page-wrapper">
        <Header />
        <div className="page-wrapper__central-wrapper">
          <AsideMenu />
          {this.props.children}
        </div>
      </div>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {oblect} children object with children nodes
*/
PageWrapper.propTypes = {
  children: React.PropTypes.object,
};

export default PageWrapper;
