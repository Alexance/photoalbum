import React, { Component } from 'react';
import { Link } from 'react-router';
import Likes from '../../likes/likes';
import endpoints from '../../../../constants/endpoint';

import './recent-photo-preview.scss';
/**
* Component for representing image preview with additional info
* @extends Component
*/
export class RecentPhotoPreview extends Component {
  /**
  * @method {decoratePhotoTitle} function for decorating photos title to more redable format
  * @param {string} undecoratedTitle prime title for decorating
  */
  decoratePhotoTitle(undecoratedTitle) {
    return undecoratedTitle.replace(/-/g, ' ');
  }
  /**
  * @method {ecoratePhotoDate} function for decorating photos date to more redable format
  * @param {string} undecoratedDate prime date for decorating
  */
  decoratePhotoDate(undecoratedDate) {
    return undecoratedDate.slice(0, 10);
  }

  render() {
    const photoTitle = this.decoratePhotoTitle(this.props.name);
    const photoDate = this.decoratePhotoDate(this.props.date);
    const photoPath = `${endpoints.base}${endpoints.images}${this.props.id}`;
    const routerPath = `${endpoints.photo}${this.props.id}`;
    return (
      <li key={this.props.id} className="recent-photo-preview">
        <div className="recent-photo-preview__photo-wrapper">
          <Link to={routerPath}>
            <img className="recent-photo-preview__photo" src={photoPath} alt={this.props.name} />
          </Link>
        </div>
        <div className="recent-photo-preview__photo-info">
          <Link className="photo-info__title-link" to={routerPath}>
            <h3 className="photo-info__title">{photoTitle}</h3>
          </Link>
          <div className="photo-info__date-and-likes-wrapper">
            <Likes
              count={this.props.likes}
              id={this.props.id}
            />
            <span className="photo-info__date">{photoDate}</span>
          </div>
        </div>
      </li>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {string} id image's id
* @property {string} name image's name
* @property {string} date image's date
* @property {number} likes image's likes value
*/
RecentPhotoPreview.propTypes = {
  id: React.PropTypes.string,
  name: React.PropTypes.string,
  date: React.PropTypes.string,
  likes: React.PropTypes.number,
};

export default RecentPhotoPreview;
