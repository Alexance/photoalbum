import React, { Component } from 'react';
import { Link } from 'react-router';
import endpoints from '../../../../constants/endpoint';
import Likes from '../../likes/likes';

import './photo-preview.scss';
/**
* Component for representing image preview with additional info
* @extends Component
*/
export class PhotoPreview extends Component {
  /**
  * render
  * @return {PhotoPreview} markup
  */
  render() {
    const photoPath = `${endpoints.base}${endpoints.images}${this.props.id}`;
    const routerPath = `${endpoints.photo}${this.props.id}`;
    const routerSettingsPath = `${endpoints.settings}${this.props.id}`;
    return (
      <article className="photo-preview">
        <Link className="homepage__photo-prew" to={routerPath}>
          <img className="photo-preview__img" src={photoPath} alt={this.props.name}/>
        </Link>
        <Link className="photo-preview__edit-lable" to={routerSettingsPath}>
          Edit
        </Link>
        <div className="photo-preview__info">
          <span className="photo-preview__img-date">{this.props.date}</span>
          <Likes
            count={this.props.likes}
            id={this.props.id}
          />
        </div>
      </article>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {number} id image's id
* @property {number} likes image's likes value
* @property {string} name image's name
* @property {string} date image's date
*/
PhotoPreview.propTypes = {
  id: React.PropTypes.number,
  likes: React.PropTypes.number,
  name: React.PropTypes.string,
  date: React.PropTypes.string,
};

export default PhotoPreview;
