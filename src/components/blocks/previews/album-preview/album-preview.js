import React, { Component } from 'react';
import endpoints from '../../../../constants/endpoint';

import './album-preview.scss';
/**
* Component for representing album's preview with additional info
* @extends Component
*/
export class AlbumPreview extends Component {
  render() {
    const previewPath = `${endpoints.base}${endpoints.images}${this.props.preview}`;
    return (
      <article className="album-preview">
        <img className="album-preview__img" src={previewPath} alt="album-preview"/>
        <div className="album-preview__info">
          <h3 className="album-preview__name">{this.props.name}</h3>
          <p className="album-preview__description">{this.props.description}</p>
        </div>
      </article>
    );
  }
}

/**
* propTypes
* @type {object} propTypes
* @property {string} name album's name
* @property {string} preview album's preview
* @property {string} description album's description
*/
AlbumPreview.propTypes = {
  name: React.PropTypes.string,
  preview: React.PropTypes.string,
  description: React.PropTypes.string,
};

export default AlbumPreview;
