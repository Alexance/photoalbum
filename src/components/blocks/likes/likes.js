import React, { Component } from 'react';
import autobind from 'autobind-decorator';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { likePhoto, dislikePhoto } from '../../../actions/photos';

import './likes.scss';
/**
 * Likes component for counting and incrementing image's likes
 * @extends Component
 */
export class Likes extends Component {
  constructor(props) {
    super(props);
    /**
    * @type {object}
    * @property {boolean} isLiked
    */
    this.state = {
      isLiked: false,
    };
  }
  /**
  * handle like button click
  * @param {SytheticEvent} e
  */
  @autobind
  onLikeBtnClick(e) {
    const { isLiked } = this.state;
    const { id, likePhoto, dislikePhoto } = this.props;
    e.preventDefault();
    if (isLiked) {
      dislikePhoto(id);
    } else {
      likePhoto(id);
    }
    this.setState({ isLiked: !isLiked });
  }
  /**
  * render
  * @return {Likes} markup
  */
  render() {
    return (
     <div className="likes">
        <a href="#" className="likes__like-btn" onClick={this.onLikeBtnClick}>Like</a>
        <span className="likes__likes-counter">{this.props.count}</span>
     </div>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {number} count image's likes value
* @property {string} id image's id
* @property {function} likePhoto action for increacing likes by one
* @property {function} dislikePhoto action for reducing likes by one
*/
Likes.propTypes = {
  count: React.PropTypes.number,
  id: React.PropTypes.string,
  likePhoto: React.PropTypes.func,
  dislikePhoto: React.PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ likePhoto, dislikePhoto }, dispatch);
}

export default connect(null, mapDispatchToProps)(Likes);
