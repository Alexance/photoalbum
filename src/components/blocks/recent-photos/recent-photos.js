import React, { Component } from 'react';
import { connect } from 'react-redux';
import RecentPhotoPreview from '../previews/recent-photo-preview/recent-photo-preview';

import './recent-photos.scss';
/**
* @constant value of photos representing by this component
* @type {number}
*/
const RECENT_PHOTOS_VALUE = 3;
/**
 * Component for holding recent photos
 * @extends Component
 */
export class RecentPhotos extends Component {
  render() {
    return (
      <div className="recent-photos">
        <h2 className="recent-photos__title">Recent photos</h2>
        <ul className="recent-photos__list">
          {
            this.props.photos.map(photo => (
               <RecentPhotoPreview
                 key={photo.getId()}
                 id={photo.getId()}
                 name={photo.getName()}
                 date={photo.getCreationDate()}
                 likes={photo.getLikes()}
               />
              ))
          }
        </ul>
      </div>
    );
  }
}

/**
* propTypes
* @type {object}
* @property {object} photos object of photos data
*/

RecentPhotos.propTypes = {
  photos: React.PropTypes.object,
};

function mapStateToProps(state) {
  return {
    photos: state.photos.getRecentPhotos(RECENT_PHOTOS_VALUE),
  };
}

export default connect(mapStateToProps, null)(RecentPhotos);
