import React, { Component } from 'react';
import Search from '../../blocks/search/search';
import RecentPhotos from '../../blocks/recent-photos/recent-photos';
import UploadBtn from '../../blocks/upload-btn/upload-btn';

import './aside-menu.scss';

/**
 * Aside menu component with additional tools
 * @extends Component
 */
export class AsideMenu extends Component {
  /**
  * render
  * @return {AsideMenu} markup
  */
  render() {
    return (
      <aside className="aside-menu">
         <Search />
         <UploadBtn />
         <RecentPhotos />
      </aside>
    );
  }
}

export default AsideMenu;
