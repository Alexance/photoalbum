import React, { Component } from 'react';
import { Link } from 'react-router';
import logo from '../../../../public/logo.png';

import './header.scss';
/**
 * Header component with logo and main navigation
 * @extends Component
 */
export class Header extends Component {
  /**
  * render
  * @return {Header} markup
  */
  render() {
    return (
      <header className="main-header">
         <h1 className="main-header__logo">
           <img className="main-header__logo-picture" src={logo} alt="logo"/>
         </h1>
         <nav className="main-nav">
           <Link to="/home" className="main-nav__nav-link" activeClassName="active-nav-link">Home</Link>
           <Link to="/albums" className="main-nav__nav-link" activeClassName="active-nav-link">Albums</Link>
           <Link to="/categories" className="main-nav__nav-link" activeClassName="active-nav-link">Categories</Link>
         </nav>
      </header>
    );
  }
}


export default Header;
