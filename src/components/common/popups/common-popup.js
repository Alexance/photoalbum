import React, { Component } from 'react';
import autobind from 'autobind-decorator';

import './common-popup.scss';
/**
* CommonPopup component for wrapping different popups
* @extends Component
*/
export class CommonPopup extends Component {
  /**
  * @method {closePopupHandler} handle close button click and clicks outside popup's content
  * @param {SytheticEvent} e
  */
  @autobind
  closePopupHandler(e) {
    e.preventDefault();
    if (e.target !== e.currentTarget) return;
    this.props.hidePopup(this.props.id);
  }

  /**
  * @method {render}
  * @return {CommonPopup} markup or {null}
  */
  render() {
    if (!this.props.isVisible) return null;
    return (
      <div id={this.props.id} className="common-popup">
        <div className="common-popup__content">
          {this.props.children}
          <div className="common-popup__close-btn-wrapper">
            <a className="common-popup__close-btn" href="#" onClick={this.closePopupHandler}>X</a>
          </div>
        </div>
      </div>
    );
  }
}
/**
* propTypes
* @type {object}
* @property {string} id popup's id
* @property {boolean} isVisible popups's visibility
* @property {function} hidePopup action for hide popup
*/
CommonPopup.propTypes = {
  id: React.PropTypes.string,
  isVisible: React.PropTypes.bool,
  hidePopup: React.PropTypes.func,
  children: React.PropTypes.object,
};

export default CommonPopup;
