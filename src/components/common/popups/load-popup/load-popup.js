import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import autobind from 'autobind-decorator';
import { uploadAndFetchPhotos } from '../../../../actions/photos';

import './load-popup.scss';
/**
* LoadPopup component for uploading files
* @extends Component
*/
export class LoadPopup extends Component {
  /**
  * @method {handleFileLoadInputChange} handle changes of file load input
  * @param {SytheticEvent} e
  */
  @autobind
  handleFileLoadInputChange(e) {
    const file = e.currentTarget.files[0];
    this.handleFileLoad(file);
  }
  /**
  * @method {handleFileLoad} handle drag file to upload it and fetch all the photos data from the server
  * @param {object} file
  */
  @autobind
  handleFileLoad(file) {
    if (!file) return;
    const myFormData = new FormData();
    myFormData.append('file', file);
    this.props.uploadAndFetchPhotos(myFormData);
  }
  /**
  * @method {onDragEnter} handle drag file to upload
  * @param {SytheticEvent} e
  */
  @autobind
  onDragEnter(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  /**
  * @method {onDragOver} handle drag over file to upload
  * @param {SytheticEvent} e
  */
  @autobind
  onDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  /**
  * @method {onDrop} handle drop file to upload into dropbox
  * @param {SytheticEvent} e
  */
  @autobind
  onDrop(e) {
    e.stopPropagation();
    e.preventDefault();

    const dt = e.dataTransfer;
    const file = dt.files[0];
    this.handleFileLoad(file);
  }

  render() {
    return (
      <div className="load-popup">
        <div className="load-popup__dropbox-load-area" onDragEnter={this.onDragEnter}
             onDragOver={this.onDragOver} onDrop={this.onDrop}
        >
           <p>Drag images here</p>
        </div>
        <input className="load-popup__file-input" type="file"
               id="file-load-input" name="photo" onChange={this.handleFileLoadInputChange} />
        <label htmlFor="file-load-input" className="load-popup__file-input-label">
          <span className="file-input-label__btn">Browse</span>
          <span className="file-input-label__text">or drag images here</span>
        </label>
        <input className="load-popup__url-input" type="url" placeholder="paste image or url" />
      </div>
    );
  }
}
/**
* propTypes
* @type {object} propTypes
* @property {function} uploadAndFetchPhotos action for uploading photos to the server and fetching photo's data
*/
LoadPopup.propTypes = {
  uploadAndFetchPhotos: React.PropTypes.func,
};

function mapDispatchToProps(dispatch) {
  return {
    uploadAndFetchPhotos: bindActionCreators(uploadAndFetchPhotos, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(LoadPopup);
