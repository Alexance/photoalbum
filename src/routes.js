import React from 'react';
import { Route, IndexRedirect } from 'react-router';
import Albums from './components/pages/albums/albums';
import Categories from './components/pages/categories/categories';
import Homepage from './components/pages/homepage/homepage';
import Photo from './components/pages/photo/photo';
import Photoalbum from './components/photoalbum';
import PhotoSettings from './components/pages/photo-settings/photo-settings';


export default function createRoutes() {
  return (
    <Route path="/" component={Photoalbum}>
      <IndexRedirect to="/home" />
      <Route path="/home" component={Homepage} />
      <Route path="/categories" component={Categories} />
      <Route path="/albums" component={Albums} />
      <Route path="/photo/:imgName" component={Photo} />
      <Route path="/settings/:imgName" component={PhotoSettings} />
    </Route>
  );
}
